STILL

Only Adam knows the sadness of doing something so wrong
Time softens the edges like a river
It gently smooths it
Time takes time

I can start to move forward
The past is forgiven
You have always be in my heart at the center
Will you know how much I love you - STILL
Forever start a long time ago.

I want it all
Will take the little pieces
Whatever is given
The heart is at peace

Tomorrow is going to be great
Today has already won
Holding a piece of heaven - STILL

Learning to love
Where love comes freely
No rush, no pressure, it springs forward
It flows in abundance and easily like a spring - STILL

A heart can be given only once
It can love again but not the same
The heart is designed to be faithful to it's true love
To love its one and only
It has only longed for only you

It can not be given back
Taken back it can not
True love does exist
Some are lucky enough to feel it
True loves is true
Connection only happens once
Few get to feel this

It is heaven.
Your heart skips, your breath is taken away, time slows, eyeys wide open.
How else does one describe
So amazing, incredible. Indescribable.

The touch, the hug
Everything stop for the moment
So excited, ecstasy, so scared
Two hearts blend at one moment

Prayed so hard for that moment
It was in the eyes
It happened in a flash
So many prayers answered right in front of everyone
They witnessed a miracle
Did they know
Two hearts collide and become one again
A rush of every emotion

The spring was locked shut
To painful to open even for a glass
My heart cried only once
It was the greatest loss
Only three things could open it - STILL

Only you and Him could even open the spring
Why did He do this
What was His reason
His touch could be felt
He never test
What is He whispering

What's the bigger picture
I know He loves me
I know He loves you
He love us

Connection Invisible but felt
As real as the air we breath
Not seen but the heart sees
It knows it's one and only
It skips a beat and pounds like thunder
All the colors
Intensity so real
Consumed - STILL

Something to share
So much time
How much has been learned
A deeper understanding of so much
Time to talk with purity of heart


